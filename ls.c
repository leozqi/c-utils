/************************************************
 *
 * Simple implementations of common CLI utils.
 *
 * @file       ls.c
 * @author     Leo Qi <leo@leozqi.com>
 * @version    0.1
 * @license    Apache-2.0
 *
 ***********************************************/

#include <stdio.h>
#include <stdbool.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <limits.h>
#include <sys/stat.h>
#include <string.h>

#ifndef PATH_MAX
#define PATH_MAX 255
#endif

struct flags {
	bool a;
	bool l;
	bool m;
};

static char const *_get_mode(struct stat *info)
{
	char *s = malloc(16 + sizeof(""));

	if (S_ISDIR(info->st_mode)) {
		s[0] = 'd';
	} else if (S_ISBLK(info->st_mode)) {
		s[0] = 'b';
	} else if (S_ISCHR(info->st_mode)) {
		s[0] = 'c';
	} else if (S_ISLNK(info->st_mode)) {
		s[0] = 'l';
	} else if (S_ISFIFO(info->st_mode)) {
		s[0] = 'p';
	} else {
		s[0] = '-';
	}

	s[1]  = info->st_mode & S_IRUSR ? 'r' : '-';
	s[2]  = info->st_mode & S_IWUSR ? 'w' : '-';
	s[3]  = info->st_mode & S_IXUSR ? 'x' : '-';
	s[4]  = info->st_mode & S_IRGRP ? 'r' : '-';
	s[5]  = info->st_mode & S_IWGRP ? 'w' : '-';
	s[6]  = info->st_mode & S_IXGRP ? 'x' : '-';
	s[7]  = info->st_mode & S_IROTH ? 'r' : '-';
	s[8]  = info->st_mode & S_IWOTH ? 'w' : '-';
	s[9]  = info->st_mode & S_IXOTH ? 'x' : '-';
	s[10] = '\0';

	return (char const *) s;
}

static int _ls(char const *dir, struct flags const f)
{
	/* <dirent.h>
	 *
	 * struct dirent {
	 *         ino_t           d_ino;
	 *         _uint16_t       d_reclen;
	 *         _uint8_t        d_type;
	 *         _uint8_t        d_namelen;
	 *         char            d_name[<implementation>]; // eg. 255+1
	 * }
	 */
	struct dirent *d;

	char *buffer = malloc(PATH_MAX + 1);
	struct stat *info = malloc(sizeof(struct stat));

	/*
	 * <dirent.h>
	 *
	 * opendir(const char *filename);
	 */
	DIR *dh = opendir(dir);
	bool first = true;

	if (!dh) {
		switch(errno) {
		case ENOENT:
			fputs("ls.c: directory does not exist\n", stderr);
			break;
		default:
			fputs("ls.c: unable to read directory\n", stderr);
		}
		return errno;
	} 

	while ((d = readdir(dh))) {
		if ((!f.a) && d->d_name[0] == '.')
			continue;

		if (first) {
			first = false;
		} else {
			if (f.l) {
				fputs("\n", stdout);
			} else if (f.m) {
				fputs(", ", stdout);
			} else {
				fputs(" ", stdout);
			}
		}

		if (!f.l) {
			fputs(d->d_name, stdout);
		} else {
			sprintf(buffer, "%s/%s", dir, d->d_name);

			if (!buffer) {
				perror("ls.c: realpath failed");
				exit(EXIT_FAILURE);
			}

			if (lstat(buffer, info) < 0) {
				perror("ls.c: lstat failure");
				fprintf(stderr, "%s %u", buffer, errno);
				exit(EXIT_FAILURE);
			}
			fprintf(stdout, "%10s  %s", _get_mode(info), d->d_name);
			/*
			fprintf(stdout, "%12s %4s %8s %5s %5s %2s %3s %6s %25s",
				_get_mode(info),
			);*/
		}
	}
	fputs("\n", stdout);
	return 0;
}

static void _add_flags(struct flags *f, char const *c)
{
	while (*c) {
		switch (*c) {
		case 'a':
			f->a = true;
			break;
		case 'l':
			f->l = true;
			break;
		case 'm':
			f->m = true;
			break;
		}
		c++;
	}

}

int main(int argc, char const *argv[])
{
	size_t i = 0;
	size_t count = 0;
	struct flags f = {
		false,
		false,
		false
	};

	for (i = 1; i < (size_t)argc; ++i) {
		if (argv[i][0] != '-') {
			count++;
		} else {
			_add_flags(&f, (char const *)argv[i]);
		}
	}

	if (!count) {
		_ls(realpath(".", NULL), f);
	} else {
		for (i = 1; i < (size_t)argc; ++i) {
			if (argv[i][0] == '-')
				continue;

			if (count > 1)
				fprintf(stdout, "%s:\n", argv[i]);

			_ls(realpath(argv[i], NULL), f);

			if ((count > 1) && (i != count))
				fputs("\n", stdout);
		}
	}

	return 0;
}
